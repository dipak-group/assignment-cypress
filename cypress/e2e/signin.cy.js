describe("Sign in", () => {
    beforeEach(() => {
      cy.visit("https://pressbooks.pub/auth/?action=signin");
     });
     it ("should sign in with valid email and password",()=>{
        cy.get(':nth-child(1) > label').type("premdhakal874@gmail.com")
        cy.get('#password').type("Dipak@12345678")
        cy.get('button').click()

       })
       it ("should sign in with valid email and wrong password",()=>{
        cy.get(':nth-child(1) > label').type("premdhakal874@gmail.com")
        cy.get('#password').type("Dipak12345678")
        cy.get('button').click()

})
it ("should sign in with uppercase letter in email",()=>{
    cy.get(':nth-child(1) > label').type("PREMDHAKAL874@gmail.com")
    cy.get('#password').type("Dipak@12345678")
    cy.get('button').click()

})
it ("should sign in with uppercase letter in password",()=>{
    cy.get(':nth-child(1) > label').type("premdhakal874@gmail.com")
    cy.get('#password').type("DIPAK@12345678")
    cy.get('button').click()

})
it ("should sign in with email @@ in email field",()=>{
    cy.get(':nth-child(1) > label').type("premdhakal874@@gmail.com")
    cy.get('#password').type("Dipak@12345678")
    cy.get('button').click()

})
})