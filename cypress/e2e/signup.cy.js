describe("Sign up", () => {
  beforeEach(() => {
    cy.visit("https://pressbooks.pub/auth/?action=signup");
   });

  it("should sign up with valid email", () => {
    cy.get(':nth-child(1) > label').type("dhakaldipak286@gmail.com")
    cy.get('#new-pass').type("12345")
    cy.get('button').click()
  })
  it("should sign up with invalid email", () => {
    cy.get(':nth-child(1) > label').type("dhakaldipak00@gmail.com")
    cy.get('#new-pass').type("12345")
    cy.get('button').click()
})
it("should sign up with valid email as successfully", () => {
  cy.get(':nth-child(1) > label').type("dhakaldipak286@gmail.com")
  cy.get('#new-pass').type("Dipak12345")
  cy.get('button').click()
})
it("should sign up without @ in email field", () => {
  cy.get(':nth-child(1) > label').type("dhakaldipak286gmail.com")
  cy.get('#new-pass').type("Dipak12345")
  cy.get('button').click()
})
it("should sign up with @@ in email field", () => {
  cy.get(':nth-child(1) > label').type("dhakaldipak286@@gmail.com")
  cy.get('#new-pass').type("Dipak12345")
  cy.get('button').click()
})
it("should sign up with valid email and password", () => {
  cy.get(':nth-child(1) > label').type("premdhakal874@gmail.com")
  cy.get('#new-pass').type("Dipak@12345678")
  cy.get('button').click()
})
it("should sign up with unregister email ", () => {
  cy.get(':nth-child(1) > label').type("applemango@gmail.com")
  cy.get('#new-pass').type("apple@Mango1")
  cy.get('button').click()
})
})